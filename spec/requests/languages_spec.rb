require 'rails_helper'

RSpec.describe "Languages", type: :request do
  describe "GET /index" do
    it "returns http success" do
      get "/languages/index"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /show" do
    it "returns http success" do
      get "/languages/show"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/languages/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /edit" do
    it "returns http success" do
      get "/languages/edit"
      expect(response).to have_http_status(:success)
    end
  end

end

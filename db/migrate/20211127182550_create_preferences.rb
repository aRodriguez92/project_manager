class CreatePreferences < ActiveRecord::Migration[6.1]
  def change
    create_table :preferences do |t|
      t.references :user, foreign_key: true
      t.references :language, foreign_key: true
      t.string :time_zone, default: 'Mexico City'

      t.timestamps
      t.timestamp :deleted_at
    end

    add_index :preferences, :deleted_at
  end
end

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.1'

gem 'audited'
gem 'aws-sdk-s3'
gem 'bootsnap', '>= 1.4.4', require: false # Boot large ruby/rails apps faster
gem 'bootstrap', '~> 5.1.3'
gem 'devise', '~> 4.8' # Flexible authentication solution for Rails
gem 'devise_invitable', '~> 2.0.5' # Allows invitations to be sent for joining
gem 'devise_masquerade', '~> 1.2'
gem 'email_validator', '~> 2.2' # Email validator for Rails and ActiveModel
gem 'enum_help'
gem 'friendly_id', '~> 5.4.0'
gem 'hotwire-rails'
gem 'i18n-timezones'
gem 'image_processing', '~> 1.2'
gem 'jbuilder', '~> 2.11' # Create JSON structures via a Builder-style DSL
gem 'local_time'
gem 'mini_magick'
gem 'name_of_person'
gem 'noticed'
gem 'paranoia'
gem 'pg', '~> 1.1' # Pg is the Ruby interface to the PostgreSQL RDBMS
gem 'puma', '~> 5.5.2' # Puma is a simple, fast, threaded, and highly concurrent HTTP 1.1 server
gem 'rails', '~> 6.1.4', '>= 6.1.4.1' # Ruby on Rails
gem 'redis', '~> 4.5.1' # Redis ruby client
gem 'roo'
gem 'roo-xls'
gem 'sass-rails', '>= 6' # Use SCSS for stylesheets
gem 'simple_form'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby] # Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'webpacker', '~> 5.4.3' # Use webpack to manage app-like JavaScript modules in Rails

group :development do
  gem 'better_errors', '~> 2.9' # Provides a better error page for Rails and other Rack apps
  gem 'binding_of_caller'
  gem 'brakeman', '~> 5.1', require: false # Brakeman detects security vulnerabilities in Ruby on Rails applications via static analysis
  gem 'bundler-audit', '~> 0.9' # bundler-audit provides patch-level verification for Bundled apps
  gem 'guard', '~> 2.18', require: false # Guard is a command line tool to easily handle events on file system modifications
  gem 'guard-livereload', '~> 2.5', require: false # Guard::LiveReload automatically reloads your browser when 'view' files are modified
  gem 'i18n-tasks', '~> 0.9.34' # Helpers to find and manage missing and unused translations
  gem 'letter_opener_web'
  gem 'listen', '~> 3.7', require: false # Helps 'listen' to file system modifications events (also used by other gems like guard)
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'web-console', '~> 4.1' # Rails Console on the Browser
end

group :development, :test do
  gem 'annotate'
  gem 'bullet', '~> 6.1' # help to kill N+1 queries and unused eager loading
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara', '~> 3.36.0' # Capybara is an integration testing tool for rack based web applications
  gem 'faker', '~> 2.19' # A library for generating fake data such as names, addresses, and phone numbers
  gem 'pry', '~> 0.14' # An IRB alternative and runtime developer console
  gem 'pry-rails', '~> 0.3' # Use Pry as your rails console
  gem 'rspec-rails', '~> 5.0' # rspec-rails is a testing framework for Rails 3+
  gem 'rubocop', '~> 1.22', require: false # Automatic Ruby code style checking tool
  gem 'rubocop-gitlab-security'
  gem 'rubocop-performance', '~> 1.11', require: false # A collection of RuboCop cops to check for performance optimizations in Ruby code
  gem 'rubocop-rails', '~> 2.12', require: false # Automatic Rails code style checking tool
  gem 'rubocop-rspec', '~> 2.5', require: false # Code style checking for RSpec files
end

group :test do
  gem 'database_cleaner'
  gem 'factory_bot_rails', '~> 6.2' # factory_bot is a fixtures replacement with a straightforward definition syntax, support for multiple build strategies
  gem 'fuubar'
  gem 'rails-controller-testing'
  gem 'shoulda-matchers', '~> 5.0.0', require: false # Simple one-liner tests for common Rails functionality
  gem 'simplecov', '~> 0.21.2', require: false # Code coverage with a powerful configuration library and automatic merging of coverage across test suites
  gem 'webdrivers', '~> 5.0' # Run Selenium tests more easily with install and updates for all supported webdrivers
end


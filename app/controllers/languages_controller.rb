class LanguagesController < ApplicationController
  before_action :set_language, only: %i[:show :edit :update :destroy]

  def index
    @languages = Language.all
  end

  def show; end

  def new
    @language = Language.new
  end

  def edit; end

  def create
    @language = Language.new(language_params)
    if @language.save
      redirect_to languages_path
    else
      render :new
    end
  end

  private

  def language_params
    params.require(:language).permit(:code, :name)
  end

  def set_language
    @language = Language.friendly.find(params[:id])
  end
end

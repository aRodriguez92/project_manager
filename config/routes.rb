Rails.application.routes.draw do
  resources :languages
  devise_for :users

  get 'home/index'
  root to: 'home#index'
end
